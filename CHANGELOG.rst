^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package thin_navigation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.0.3 (2017-05-22)
------------------
* added install targets
* Contributors: Marc Hanheide

0.0.2 (2017-05-22)
------------------
* added std_srvs as dep
* package.xml edited online with Bitbucket
* Contributors: Marc Hanheide

0.0.1 (2017-05-18)
------------------
* changed maintainer to Marc Hanheide
* Merge branch 'master' of https://bitbucket.org/ggrisetti/thin_navigation
* Fixed isnan not compiling on ubuntu 16.04
* minor fixes
* refactoring of parameters
* minor fixes
* updated parameters
* adjusted dynamic reconfigure
* moved ROS parameters settings in a separate function
* added dynamic reconfigure feature
* managing goal preemption and cancel
* cancel goal to be fixed
* added use_gradient_controller flag
* added empty CB to cancel goals
* TODO make these chamged values parametric
* added map_service_id parameter
* monster bug in distance map fixed
  Thanks to Federico Nardi, that now will get write permissions :)
* localizer ranges are now fresh like a field flower
* changed parameter initial_pose_theta to initial_pose_a in thin_localizer
* minor changes
* some minor changes
* attractor computed from the plan
* adjustment for real robot
* Reduced Cpu Usage
* gradient controller minor changes
* Gradient controller
* test gradient controller
* test gradient controller
* gradient test
* gradient test
* gradient test
* gradient_controller added
* parameter adjustment
* slow down when dynamic obstacle detected
* new topic nearest_object_distance
* some parameter adjustment and global path added
* added acceleration control
* code refactoring
* code refactoring
* first code refactoring
* some minor changes
* allow robot to exit from wall
* README.md edited online with Bitbucket
* contributors.txt edited online with Bitbucket
* README.md edited online with Bitbucket
* some parameter adjustment
* same parameter for GO and NEAR
* some parameter adjustment
* Merge branch 'master' of https://bitbucket.org/ggrisetti/thin_navigation
* no more recursive function
* README.md edited online with Bitbucket
* consider robot radius in chosing plan
* first public commit
* Contributors: Cristiano Gennari, Federico, Giorgio Grisetti, Imperoli, Luca Iocchi, Marc Hanheide, Mayte Lazaro, luca_iocchi, turtlebot
